from django.shortcuts import render

# Create your views here.
def profile(request):
    return render(request, 'profile.html')

def experience(request):
    return render(request, 'experience.html')

def reachme(request):
    return render(request, 'reach-me.html')

def home(request):
    return render(request, 'home.html')