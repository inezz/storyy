from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('reach-me/', views.reachme, name='reachme'),
    path('experience/', views.experience, name='experience'),
]